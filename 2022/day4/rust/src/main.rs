#![warn(clippy::pedantic)]

use std::io::{stdin, Read};

use aoc::SectionRange;

fn main() {
    let mut data = String::new();
    stdin().read_to_string(&mut data).unwrap();

    let pairs: Vec<_> = data
        .lines()
        .map(|line| -> (SectionRange<u64>, SectionRange<u64>) {
            let (left, right) = line.split_once(',').unwrap();
            let left = left.parse().unwrap();
            let right = right.parse().unwrap();

            (left, right)
        })
        .collect();

    println!(
        "{}",
        pairs
            .iter()
            .filter(|&&(l, r)| l.encompasses(&r) || r.encompasses(&l))
            .count()
    );

    println!("{}", pairs.iter().filter_map(|&(l, r)| l & r).count());
}
