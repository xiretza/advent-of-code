#!/usr/bin/env python

import sys
import itertools

def parse(line):
    inputs, outputs = [[num.strip() for num in nums.strip().split(' ')] for nums in line.split('|')]
    return (inputs, outputs)

if __name__ == '__main__':
    inp = [parse(i) for i in sys.stdin.readlines()]

    permutations = list(''.join(s) for s in itertools.permutations('abcdefg'))
    segments = [
        'abcefg',
        'cf',
        'acdeg',
        'acdfg',
        'bcdf',
        'abdfg',
        'abdefg',
        'acf',
        'abcdefg',
        'abcdfg',
    ]

    acc1 = 0
    acc2 = 0
    for (inputs, outputs) in inp:
        acc1 += sum(len(output) in (2,3,4,7) for output in outputs)

        for permutation in permutations:
            trans = str.maketrans(permutation, 'abcdefg')

            def translate_segments(digit):
                return ''.join(sorted(digit.translate(trans)))

            if all(translate_segments(digit) in segments for digit in inputs):
                acc2 += int(''.join(
                    str(segments.index(
                        translate_segments(digit)
                    )) for digit in outputs
                ))
                break
    print(acc1)
    print(acc2)
