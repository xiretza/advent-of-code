use std::{
    cmp::Reverse,
    io::{stdin, Read},
};

fn main() {
    let mut data = String::new();
    stdin().read_to_string(&mut data).unwrap();

    let lines: Vec<_> = data.lines().collect();
    let mut elves: Vec<_> = lines
        .split(|s| s.is_empty())
        .map(|entries| {
            entries
                .iter()
                .map(|c| c.parse::<u64>().unwrap())
                .sum::<u64>()
        })
        .collect();
    elves.sort_by_key(|&s| Reverse(s));

    println!("{}", elves[0]);
    println!("{}", &elves[..3].iter().sum::<u64>());
}
