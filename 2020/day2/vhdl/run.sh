#!/bin/bash

source "$COMMON_DIR/vhdl_run.sh"

cd "$(dirname "${BASH_SOURCE[0]}")"

DUT_OUTPUT_WIDTH=12
test_synth day2 parser.vhdl verifier.vhdl top.vhdl
