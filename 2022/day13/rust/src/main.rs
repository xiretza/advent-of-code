#![warn(clippy::pedantic)]

use nom::Parser;
use std::io::{stdin, Read};

use nom::{
    branch::alt, bytes::complete::tag, character::complete::u32, multi::separated_list0,
    sequence::delimited, IResult,
};

#[derive(Debug, Clone, PartialEq, Eq)]
enum Data {
    List(Vec<Data>),
    Number(usize),
}

impl PartialOrd for Data {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Data {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match (self, other) {
            (Data::List(l), Data::List(r)) => l.as_slice().cmp(r),
            (Data::List(l), r @ Data::Number(_)) => l.iter().cmp([r]),
            (l @ Data::Number(_), Data::List(r)) => [l].into_iter().cmp(r.iter()),
            (Data::Number(l), Data::Number(r)) => l.cmp(r),
        }
    }
}

fn parse_data(s: &str) -> IResult<&str, Data, ()> {
    alt((
        delimited(tag("["), separated_list0(tag(","), parse_data), tag("]")).map(Data::List),
        u32.map(|i| Data::Number(usize::try_from(i).unwrap())),
    ))(s)
}

fn main() {
    let mut data = String::new();
    stdin().read_to_string(&mut data).unwrap();

    let pairs: Vec<_> = data
        .split("\n\n")
        .map(|pair| {
            let (left, right) = pair.split_once('\n').unwrap();
            (parse_data(left).unwrap().1, parse_data(right).unwrap().1)
        })
        .collect();

    let num_sorted: usize = pairs
        .iter()
        .enumerate()
        .filter_map(|(i, (left, right))| (left <= right).then_some(i + 1))
        .sum();

    println!("{}", num_sorted);

    let dividers = [2, 6]
        .into_iter()
        .map(|i| Data::List(vec![Data::List(vec![Data::Number(i)])]));

    let mut sorted: Vec<_> = pairs
        .into_iter()
        .flat_map(|(left, right)| [left, right])
        .collect();
    sorted.extend(dividers.clone());
    sorted.sort();

    println!(
        "{}",
        dividers
            .map(|div| sorted.binary_search(&div).unwrap() + 1)
            .product::<usize>()
    );
}
