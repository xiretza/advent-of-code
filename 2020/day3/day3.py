#!/usr/bin/env python

import sys

def count_trees(right, down, lines):
    n = 0
    for i, line in enumerate(lines[::down]):
        c = line[(i*right) % len(line)]
        if c == '#':
            n += 1
    return n

if __name__ == '__main__':
    lines = [line.strip() for line in sys.stdin.readlines()]

    print(count_trees(3, 1, lines))

    hits_multiplied = 1
    for slope in ((1,1), (3,1), (5,1), (7,1), (1,2)):
        hits_multiplied *= count_trees(*slope, lines)
    print(hits_multiplied)
