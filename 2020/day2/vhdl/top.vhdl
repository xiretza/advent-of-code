library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

entity top is
	generic (
		OUTPUT_WIDTH : positive;
		STEP : natural range 1 to 2
	);
	port (
		clk : in std_logic;
		reset : in std_logic;
		char  : in character;

		input_valid : in std_logic;

		num_verified : out unsigned(OUTPUT_WIDTH-1 downto 0) := (others => '0');
		output_valid : out std_logic
	);
end entity;

architecture behaviour of top is
	signal is_data : std_logic;
	signal num1, num2 : natural range 0 to 99;
	signal letter : character;

	signal record_end : std_logic;

	signal verified : std_logic;
begin
	parser_inst: entity work.parser
		port map (
			clk   => clk,
			reset => reset,

			record_end => record_end,
			is_data   => is_data,

			char   => char,
			input_valid => input_valid,

			num1   => num1,
			num2   => num2,
			letter => letter
		);

	generate_verifier: if step = 1 generate
		verifier_inst: entity work.verifier(step1)
			port map (
				clk => clk,
				reset => reset or record_end,
				enable => is_data and input_valid,
				num1   => num1,
				num2   => num2,
				letter => letter,
				char   => char,
				verified => verified
			);
	elsif step = 2 generate
		verifier_inst: entity work.verifier(step2)
			port map (
				clk => clk,
				reset => reset or record_end,
				enable => is_data and input_valid,
				num1   => num1,
				num2   => num2,
				letter => letter,
				char   => char,
				verified => verified
			);
	else generate
		assert false report "Bad value for ""step""" severity failure;
	end generate;

	process(clk)
	begin
		if rising_edge(clk) then
			if reset then
				num_verified <= (others => '0');
			elsif record_end and verified then
				num_verified <= num_verified + 1;
			end if;
		end if;
	end process;

	output_valid <= '1';
end architecture;
