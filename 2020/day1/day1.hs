import AoC

import Data.List
import Data.Maybe
import Control.Monad

find_n_summing :: (Num a, Eq a) => a -> Int -> [a] -> Maybe [a]
find_n_summing to = (find ((to ==) . sum) .) . replicateM

main = runAoC (fmap read <$> lines) (solution 2) (solution 3)
    where solution = product . fromJust .: find_n_summing 2020
