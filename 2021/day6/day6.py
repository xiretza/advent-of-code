#!/usr/bin/env python

import sys
import functools

@functools.cache
def spawns(days):
    acc = days//7
    days -= 9
    acc += sum(spawns(days-x*7) for x in range(days//7))
    return acc

if __name__ == '__main__':
    inp = [int(i) for i in sys.stdin.readline().split(',')]

    def do(days):
        spawns.cache_clear()
        return sum(1 + spawns(days + (6 - fish)) for fish in inp)
    print(do(80))
    print(do(256))
