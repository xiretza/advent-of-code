#!/usr/bin/env python

import sys

if __name__ == '__main__':
    x = 0
    y = 0
    z = 0
    for line in sys.stdin.readlines():
        dir, amount = line.split()
        amount = int(amount)
        if dir == 'forward':
            x += amount
            z += y * amount
        elif dir == 'down':
            y += amount
        elif dir == 'up':
            y -= amount
    print(x*y)
    print(x*z)
