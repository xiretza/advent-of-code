#!/usr/bin/env python

import time

import sys

def board_won(board):
    for row in board:
        if all(x is None for x in row):
            return True
    for i in range(len(board[0])):
        if all(row[i] is None for row in board):
            return True
    return False

if __name__ == '__main__':
    lines = [line.strip() for line in sys.stdin.readlines()]

    draws = [int(n) for n in lines[0].split(',')]

    boards = []
    for i in range(2, len(lines), 6):
        boards.append([[int(n) for n in line.split()] for line in lines[i:i+5]])

    wins = []
    for draw in draws:
        for board_idx, board in enumerate(boards):
            if board is None:
                continue
            for row in board:
                try:
                    idx = row.index(draw)
                    row[idx] = None
                except ValueError:
                    pass
            if board_won(board):
                wins.append(draw * sum(n for row in board for n in row if n is not None))
                boards[board_idx] = None

    print(wins[0])
    print(wins[-1])
