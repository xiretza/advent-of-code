#!/usr/bin/env python
import itertools
import sys
from typing import Iterable

def product(nums: Iterable[int]) -> int:
    """
    Returns the product of all elements in 'nums'
    """
    x = 1
    for num in nums:
        x *= num
    return x

def find_summing(arity: int, nums: Iterable[int]) -> int:
    return product(next(filter(lambda xs: sum(xs) == 2020, itertools.permutations(nums, r=arity))))

if __name__ == '__main__':
    nums = [int(n) for n in sys.stdin.readlines()]
    print(find_summing(2, nums))
    print(find_summing(3, nums))
