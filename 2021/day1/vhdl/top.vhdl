library ieee;
use ieee.std_logic_1164.all,
	ieee.numeric_std.all;

use work.util.all;

entity top is
	generic (
		MAX_INPUT_DIGITS : positive;
		OUTPUT_WIDTH : positive;
		STEP : natural range 1 to 2
	);
	port (
		clk : in std_logic;
		reset : in std_logic;

		char  : in character;
		input_valid : in std_logic;

		output : out unsigned(OUTPUT_WIDTH-1 downto 0);
		output_valid : out std_logic
	);
end entity;

architecture arch of top is
	function WINDOW_SIZE return positive is
	begin
		if STEP = 1 then
			return 1;
		else
			return 3;
		end if;
	end function;

	type number_t is array(MAX_INPUT_DIGITS-1 downto 0) of digit_t;
	type window_t is array(WINDOW_SIZE-1 downto 0) of number_t;

	signal current_number : number_t;
	signal window : window_t;

	signal window_count : natural range 0 to WINDOW_SIZE := 0;
begin
	process(clk)
	begin
		if rising_edge(clk) then
			if reset then
				output <= (others => '0');
				output_valid <= '1';
				window_count <= 0;
			elsif input_valid then
				output_valid <= '0';

				if char = LF then
					if window_count = WINDOW_SIZE then
						if current_number > window(window'high) then
							output <= output + 1;
						end if;
						output_valid <= '1';
					else
						window_count <= window_count + 1;
					end if;

					window <= window(window'high-1 downto 0) & current_number;
					current_number <= (others => 0);
				else
					current_number <= current_number(current_number'left-1 downto 0) & char_to_digit(char);
				end if;
			end if;
		end if;
	end process;
end architecture;
