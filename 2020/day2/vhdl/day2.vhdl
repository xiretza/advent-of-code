configuration day2 of sim is
	for aoc_stdio
		for dut_inst: dut
			use entity work.top port map (
				clk => clk,
				reset => reset,

				char => char,
				input_valid => input_valid,

				num_verified => output,
				output_valid => output_valid
			);
		end for;
	end for;
end configuration;
