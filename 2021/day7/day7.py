#!/usr/bin/env python

import sys

if __name__ == '__main__':
    positions = [int(i) for i in sys.stdin.readline().split(',')]

    def calc(movement_cost):
        return min(
            sum(movement_cost(abs(position-target)) for position in positions)
            for target in range(min(positions), max(positions)+1)
        )

    print(calc(lambda n: n))
    print(calc(lambda n: n * (n+1)//2))
