#!/usr/bin/env python

import sys

def run_game(init, iterations):
    last_seen = {}
    previous = None

    for i, n in enumerate(init):
        if previous is not None:
            last_seen[previous] = i-1
        previous = n

    for i in range(i+1, iterations):
        if previous in last_seen:
            x = i - last_seen[previous] - 1
        else:
            x = 0
        last_seen[previous] = i-1
        previous = x

    return previous

if __name__ == '__main__':
    init = [int(n) for n in sys.stdin.read().split(',')]
    print(run_game(init, 2020))
    print(run_game(init, 30000000))
