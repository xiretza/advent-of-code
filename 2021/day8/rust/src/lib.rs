#![warn(clippy::pedantic)]
#![deny(unsafe_op_in_unsafe_fn)]

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct LineResult {
    pub unique_digits: usize,
    pub number: usize,
}

pub mod v1;
pub mod v2;
pub mod v3;
