#!/usr/bin/env python

import sys

def within(a, b, x):
    return a <= x <= b

PASSPORT_FIELDS = {
        'byr': lambda s: len(s) == 4 and within(1920, 2002, int(s)),
        'iyr': lambda s: len(s) == 4 and within(2010, 2020, int(s)),
        'eyr': lambda s: len(s) == 4 and within(2020, 2030, int(s)),
        'hgt': lambda s: within(*{'cm': (150, 193), 'in': (59, 76)}[s[-2:]], int(s[:-2])),
        'hcl': lambda s: len(s) == 7 and s[0] == '#' and all(c in "0123456789abcdef" for c in s[1:]),
        'ecl': lambda s: s in ('amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'),
        'pid': lambda s: len(s) == 9 and s.isdigit(),
}

def parse_passport(line):
    d = {}
    for entry in line.split():
        k, v = entry.split(':', maxsplit=1)
        d[k] = v
    return d

def check_passport_fields_exist(passport):
    return set(PASSPORT_FIELDS.keys()) <= set(passport.keys())

def check_passport_fields_valid(passport):
    for name, spec in PASSPORT_FIELDS.items():
        try:
            valid = spec(passport[name])
        except:
            valid = False

        if not valid:
            return False
    return True

def split_on(f, it):
    current_part = []

    for el in it:
        if f(el):
            yield current_part
            current_part = []
        else:
            current_part.append(el)

    if current_part != []:
        yield current_part

if __name__ == '__main__':
    lines = [line.strip() for line in sys.stdin.readlines()]

    specs = list(' '.join(lines) for lines in split_on(lambda l: l == "", lines))

    passports = [parse_passport(spec) for spec in specs]
    print(len(list(filter(check_passport_fields_exist, passports))))
    print(len(list(filter(check_passport_fields_valid, passports))))
