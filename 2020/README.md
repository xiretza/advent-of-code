# Solutions for Advent of Code 2020

https://adventofcode.com/2020/

## Overview

|day| python | haskell | VHDL |
|---|--------|---------|------|
| 1 |  `**`  |  `**`   |      |
| 2 |  `**`  |  `**`   | `**` |
| 3 |  `**`  |  `**`   |      |
| 4 |  `**`  |  `**`   |      |
| 5 |        |  `**`   |      |
| 6 |        |  `**`   |      |
| 7 |  `**`  |         |      |
| 8 |        |  `**`   |      |
| 9 |        |  `**`   |      |
|10 |        |  `**`   |      |
|11 |  `**`  |         |      |
|12 |        |         |      |
|13 |        |  `**`   |      |
|14 |        |  `**`   |      |
|15 |  `**`  |         |      |
|16 |        |  `**`   |      |
|17 |        |  `**`   |      |
|18 |        |  `**`   |      |
|19 |        |  `**`   |      |
|20 |        |         |      |
|21 |  `**`  |         |      |
|22 |        |         |      |
|23 |        |         |      |
|24 |        |  `**`   |      |

`test.sh` can be used to run all solutions and automatically compares them to (my) puzzle inputs and the expected outputs.
