#![warn(clippy::pedantic)]
use itertools::Itertools;
use std::io::{self, BufRead};

fn count_increasing(i: impl Iterator<Item = impl Ord + Clone>) -> usize {
    i.tuple_windows().filter(|(x, y)| x < y).count()
}

fn main() {
    let numbers: Vec<usize> = io::stdin()
        .lock()
        .lines()
        .map(|l| str::parse(&l.unwrap()).unwrap())
        .collect();

    println!("{}", count_increasing(numbers.iter()));
    println!(
        "{}",
        count_increasing(
            numbers
                .windows(3)
                .map(|window| window.iter().sum::<usize>())
        )
    );
}
