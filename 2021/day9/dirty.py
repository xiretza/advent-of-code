#!/usr/bin/env python

import sys
import itertools

def walk_up(grid, x_orig, y_orig):
    rows = len(grid)
    cols = len(grid[0])
    current = grid[x_orig][y_orig]
    points = set()
    for dx in (-1, 0, +1):
        for dy in (-1, 0, +1):
            if (dx == 0) == (dy == 0):
                continue
            x = x_orig + dx
            y = y_orig + dy
            if 0 <= x < rows and 0 <= y < cols:
                if current < grid[x][y] < 9:
                    points.add((x, y))
                    points |= walk_up(grid, x, y)
    return points


if __name__ == '__main__':
    inp = [[int(n) for n in i.strip()] for i in sys.stdin.readlines()]

    rows = len(inp)
    cols = len(inp[0])
    acc = 0
    sizes = []
    for x, row in enumerate(inp):
        for y, n in enumerate(row):
            if y > 0 and row[y-1] <= n:
                continue
            if y < cols-1 and row[y+1] <= n:
                continue
            if x > 0 and inp[x-1][y] <= n:
                continue
            if x < rows-1 and inp[x+1][y] <= n:
                continue
            points = walk_up(inp, x, y)
            points.add((x, y))
            sizes.append(len(points))
            acc += n + 1
    sizes.sort()
    print(acc)
    acc2 = 1
    for x in sizes[-3:]:
        acc2 *= x
    print(acc2)
