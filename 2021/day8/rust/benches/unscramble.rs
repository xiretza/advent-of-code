use std::{fs, path::PathBuf};

use criterion::{black_box, criterion_group, criterion_main, Criterion};
use rust_2021_8::{v1, v2, v3};

pub fn criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("unscramble");

    let mut data_file = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    data_file.push("inputs/input.txt");

    let input = fs::read_to_string(data_file).unwrap();
    let lines: Vec<&str> = input.lines().collect();

    group.bench_function("v1", |b| {
        b.iter(|| {
            for line in &lines {
                let _ = v1::unscramble(black_box(line));
            }
        })
    });
    group.bench_function("v2", |b| {
        b.iter(|| {
            for line in &lines {
                let _ = v2::unscramble(black_box(line));
            }
        })
    });
    group.bench_function("v3", |b| {
        b.iter(|| {
            for line in &lines {
                let _ = v3::unscramble(black_box(line));
            }
        })
    });
    group.finish();
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
