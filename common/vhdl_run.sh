#!/usr/bin/bash

set -euo pipefail

INPUT=$(readlink --canonicalize-existing "$1")
workdir=workdir
GHDLFLAGS="--std=08 --workdir=$workdir"

mkdir -p workdir

test_synth() {
	local config_name=$1; shift

	for step in 1 2; do
		ghdl remove $GHDLFLAGS
		ghdl synth $GHDLFLAGS -gOUTPUT_WIDTH="$DUT_OUTPUT_WIDTH" -gSTEP="$step" "$COMMON_DIR/util.vhdl" "$@" -e top > "$workdir/top_syn.vhdl" 2>/dev/null
		ghdl analyze $GHDLFLAGS "$workdir/top_syn.vhdl" "$COMMON_DIR/testbench.vhdl" "$config_name.vhdl"
		ghdl elab-run $GHDLFLAGS "$config_name" -gOUTPUT_WIDTH="$DUT_OUTPUT_WIDTH" --wave="$workdir/synth$step.ghw" --ieee-asserts=disable < "$INPUT"
	done
}

test_sim() {
	local config_name=$1; shift

	ghdl remove $GHDLFLAGS
	ghdl analyze $GHDLFLAGS "$COMMON_DIR/testbench.vhdl" "$COMMON_DIR/util.vhdl" "$@" "$config_name.vhdl"
	for step in 1 2; do
		ghdl elab-run $GHDLFLAGS "$config_name" -gOUTPUT_WIDTH="$DUT_OUTPUT_WIDTH" -gSTEP="$step" --wave="$workdir/sim$step.ghw" < "$INPUT"
	done
}
