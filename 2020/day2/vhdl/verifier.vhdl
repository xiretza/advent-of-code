library ieee;
use ieee.std_logic_1164.all;

entity verifier is
	port (
		clk : in std_logic;
		reset : in std_logic;
		enable : in std_logic;

		num1, num2 : in natural range 0 to 99;
		letter : in character;

		char : in character;

		verified : out std_logic
	);
end entity;

architecture step1 of verifier is
	signal count : natural range 0 to 99;
begin
	process(clk)
	begin
		if rising_edge(clk) then
			if reset then
				count <= 0;
			elsif enable then
				if char = letter then
					count <= count + 1;
				end if;
			end if;
		end if;
	end process;

	verified <= '1' when num1 <= count and count <= num2 else '0';
end architecture;

architecture step2 of verifier is
	signal count : natural range 1 to 99;
	signal parity : std_logic;
begin
	process(clk)
	begin
		if rising_edge(clk) then
			if reset then
				count <= 1;
				parity <= '0';
			elsif enable then
				count <= count + 1;
				if (count = num1 or count = num2) and char = letter then
					parity <= not parity;
				end if;
			end if;
		end if;
	end process;

	verified <= parity;
end architecture;
