#!/usr/bin/env python
import re
import sys
from collections import Counter

r = re.compile(r'(\d+)-(\d+) (.): (.*)')

def requirements_met_1(min_required, max_required, letter, password):
    return min_required <= Counter(password)[letter] <= max_required

def requirements_met_2(first_pos, second_pos, letter, password):
    return (password[first_pos-1] == letter) != (password[second_pos-1] == letter)

def do_line(req_func, line):
    min_required, max_required, letter, password = r.match(line).groups()
    return req_func(int(min_required), int(max_required), letter, password)

if __name__ == '__main__':
    lines = list(sys.stdin.readlines())

    for func in (requirements_met_1, requirements_met_2):
        print(len(list(filter(lambda line: do_line(func, line), lines))))
