{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, DataKinds #-}

module Day17 where

import AoC
import Conway

import Data.List
import Data.Vector.Sized (Vector)
import qualified Data.Vector.Sized as V
import Data.Set (Set)
import qualified Data.Set as S

instance (Num a, Eq a) => Index (Vector n a) where
    neighbours i = filter (/= i) . V.mapM (flip map [-1, 0, 1] . (+)) $ i
    addIndex = V.zipWith (+)

run6 :: (Index i, Ord i) => Set i -> Int
run6 = S.size . (!! 6) . iterate (update (==3) (`notElem` [2, 3]))

main = runAoC (toIndices . lines) (run6 . S.fromList . map extendIndex) (run6 . S.fromList . map (extendIndex . extendIndex))
    where toIndices input = [V.fromTuple (row, col) :: Vector 2 Int | (row, line) <- zip [0..] input, (col, '#') <- zip [0..] line]
          extendIndex = V.cons 0
