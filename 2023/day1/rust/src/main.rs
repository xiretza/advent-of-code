#![warn(clippy::pedantic)]

use std::io::stdin;

const DIGIT_WORDS: [&str; 9] = [
    "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
];

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Direction {
    Forward,
    Backward,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum DigitMode {
    NumeralsOnly,
    NumeralsAndWords,
}

fn locate_digit(s: &str, direction: Direction, mode: DigitMode) -> usize {
    let locate_digit = |n: usize, word| {
        let locate = match direction {
            Direction::Forward => str::find,
            Direction::Backward => str::rfind,
        };
        let numeral = n.to_string();
        let numeral_pos = locate(s, numeral.as_str());
        let word_pos = if mode == DigitMode::NumeralsAndWords {
            locate(s, word)
        } else {
            None
        };
        [numeral_pos, word_pos].into_iter().flatten()
    };

    let positions = DIGIT_WORDS.into_iter().enumerate().flat_map(|(n, word)| {
        let n = n + 1;
        locate_digit(n, word).map(move |pos| (pos, n))
    });

    match direction {
        Direction::Forward => positions.min_by_key(|(pos, _)| *pos),
        Direction::Backward => positions.max_by_key(|(pos, _)| *pos),
    }
    .unwrap()
    .1
}

fn get_number(s: &str, mode: DigitMode) -> usize {
    let first = locate_digit(s, Direction::Forward, mode);
    let last = locate_digit(s, Direction::Backward, mode);
    first * 10 + last
}

fn get_sum(lines: &[String], mode: DigitMode) -> usize {
    lines.iter().map(|l| get_number(l, mode)).sum()
}

fn main() {
    let lines: Vec<_> = stdin().lines().map(Result::unwrap).collect();

    println!("{}", get_sum(&lines, DigitMode::NumeralsOnly));
    println!("{}", get_sum(&lines, DigitMode::NumeralsAndWords));
}
