#![warn(clippy::pedantic)]

use std::io::{stdin, Read};

use regex::Regex;

fn main() {
    let mut data = String::new();
    stdin().read_to_string(&mut data).unwrap();

    let mut lines = data.lines();

    let mut stacks = Vec::new();

    for line in &mut lines {
        if !line.contains('[') {
            lines.next();
            break;
        }

        let crates = line.as_bytes().chunks(4).map(|crate_| {
            if crate_.iter().all(|&c| c == b' ') {
                // empty space
                return None;
            }

            let crate_ = crate_.strip_suffix(b" ").unwrap_or(crate_);

            let Some([crate_]) = crate_
                .strip_prefix(b"[")
                .and_then(|crate_| crate_.strip_suffix(b"]"))
            else {
                panic!("invalid crate {:?} {}", crate_, std::str::from_utf8(crate_).unwrap());
            };

            Some(*crate_ as char)
        });

        for (i, crate_) in crates.enumerate() {
            if stacks.len() <= i {
                stacks.push(vec![]);
            }

            if let Some(crate_) = crate_ {
                stacks[i].push(crate_);
            }
        }
    }
    for stack in &mut stacks {
        stack.reverse();
    }
    let stacks = stacks;

    let mut stacks_9000 = stacks.clone();
    let mut stacks_9001 = stacks;

    let regex = Regex::new(r"move (\d+) from (\d+) to (\d+)").unwrap();
    for line in lines {
        fn get_stacks(
            stacks: &mut [Vec<char>],
            source: usize,
            dest: usize,
        ) -> (&mut Vec<char>, &mut Vec<char>) {
            let (p1, p2) = stacks.split_at_mut(usize::max(source, dest));

            if source < dest {
                (&mut p1[source], &mut p2[0])
            } else {
                (&mut p2[0], &mut p1[dest])
            }
        }

        let matches = regex.captures(line).unwrap();

        let amount: usize = matches[1].parse().unwrap();
        let source: usize = matches[2].parse().unwrap();
        let dest: usize = matches[3].parse().unwrap();

        let source = source - 1;
        let dest = dest - 1;

        {
            let (source, dest) = get_stacks(&mut stacks_9000, source, dest);
            let sourceidx = source.len() - amount;

            dest.extend(source[sourceidx..].iter().copied().rev());
            source.truncate(sourceidx);
        }

        {
            let (source, dest) = get_stacks(&mut stacks_9001, source, dest);
            let sourceidx = source.len() - amount;

            dest.extend(source[sourceidx..].iter().copied());
            source.truncate(sourceidx);
        }
    }

    for stacks in [stacks_9000, stacks_9001] {
        println!(
            "{}",
            stacks.iter().map(|s| s.last().unwrap()).collect::<String>()
        );
    }
}
