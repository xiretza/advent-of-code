#![warn(clippy::pedantic)]
use rust_2021_8::{v3::unscramble, LineResult};
use std::io::{stdin, BufRead};

fn main() {
    let result = stdin()
        .lock()
        .lines()
        .map(|s| unscramble(&s.unwrap()))
        .fold(
            LineResult {
                unique_digits: 0,
                number: 0,
            },
            |a, b| LineResult {
                unique_digits: a.unique_digits + b.unique_digits,
                number: a.number + b.number,
            },
        );

    println!("{}", result.unique_digits);
    println!("{}", result.number);
}
