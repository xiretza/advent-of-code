#!/usr/bin/env python

import sys
from collections import Counter

if __name__ == '__main__':
    acc1 = 0
    acc2 = 0
    for line in sys.stdin.readlines():
        inputs, outputs = line.split('|')
        counter = Counter(c for cs in inputs for c in cs)
        digits = [{17: 1, 25: 7, 30: 4, 34: 2, 37: 5, 39: 3, 41: 6, 42: 0, 49: 8, 45: 9}[sum(counter[c] for c in output)] for output in outputs.strip().split()]
        acc1 += sum(digit in (1, 4, 7, 8) for digit in digits)
        acc2 += int(''.join(str(digit) for digit in digits))
    print(acc1)
    print(acc2)
