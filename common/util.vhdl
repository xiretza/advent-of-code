package util is
	subtype digit_t is natural range 0 to 9;
	function char_to_digit(input : in character) return digit_t;
end package;

package body util is
	function char_to_digit(input : in character) return digit_t is
	begin
		if input >= '0' and input <= '9' then
			return character'pos(input) - character'pos('0');
		else
			return 0;
		end if;
	end function;
end package body;
