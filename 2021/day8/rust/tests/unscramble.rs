use std::path::PathBuf;

use rust_2021_8::{v1, v2, v3, LineResult};

fn test_unscramble_with_input(f: fn(&str) -> LineResult, input: &[&str]) -> LineResult {
    let mut result = LineResult {
        unique_digits: 0,
        number: 0,
    };

    for line in input {
        let LineResult {
            unique_digits,
            number,
        } = f(line);
        result.unique_digits += unique_digits;
        result.number += number;
    }

    return result;
}

fn test_unscramble(f: fn(&str) -> LineResult) {
    let test_file = |name| {
        let mut path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        path.push("inputs/");
        path.push(name);

        let input = std::fs::read_to_string(path).unwrap();
        let input: Vec<_> = input.lines().collect();
        test_unscramble_with_input(f, &input)
    };

    assert_eq!(
        test_file("example.txt"),
        LineResult {
            unique_digits: 26,
            number: 61229
        }
    );

    assert_eq!(
        test_file("input.txt"),
        LineResult {
            unique_digits: 543,
            number: 994266
        }
    );

    assert_eq!(
        test_file("large.txt"),
        LineResult {
            unique_digits: 159946,
            number: 498570828
        }
    );
}

#[test]
pub fn test_unscramble_v1() {
    test_unscramble(v1::unscramble)
}

#[test]
pub fn test_unscramble_v2() {
    test_unscramble(v2::unscramble)
}

#[test]
pub fn test_unscramble_v3() {
    test_unscramble(v3::unscramble)
}
