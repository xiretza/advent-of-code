import AoC

import qualified Data.Set as S
import           Data.Set (Set)

main = runAoC ((fmap . fmap) S.fromList . splitOnEmptyLines) part1 part2
    where part1 = countSetsFolded S.union
          part2 = countSetsFolded S.intersection
          countSetsFolded f = sum . map (S.size . foldr1 f)
