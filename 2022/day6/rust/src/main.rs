#![warn(clippy::pedantic)]

use std::io::{stdin, Read};

fn all_distinct<T: PartialEq>(slice: &[T]) -> bool {
    slice
        .iter()
        .enumerate()
        .all(|(i, x)| slice[i + 1..].iter().all(|y| y != x))
}

fn end_of_first_distinct_run<T: PartialEq>(data: &[T], len: usize) -> Option<usize> {
    data.windows(len).position(all_distinct).map(|i| i + len)
}

fn main() {
    let mut data = String::new();
    stdin().read_to_string(&mut data).unwrap();
    let data = data.as_bytes();

    println!("{:?}", end_of_first_distinct_run(data, 4).unwrap());
    println!("{:?}", end_of_first_distinct_run(data, 14).unwrap());
}
