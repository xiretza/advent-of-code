#!/usr/bin/env python

import sys

if __name__ == '__main__':
    syntax_check_score = 0
    autocomplete_scores = []
    for line in sys.stdin.readlines():
        stack = []
        corrupted = False

        for c in line.strip():
            if c in '([{<':
                stack.append({'(': ')', '[': ']', '{': '}', '<': '>'}[c])
            elif stack.pop() != c:
                syntax_check_score += {')': 3, ']': 57, '}': 1197, '>': 25137}[c]
                corrupted = True
                break

        if corrupted: continue

        autocomplete_score = 0
        for c in stack[::-1]:
            autocomplete_score *= 5
            autocomplete_score += {')': 1, ']': 2, '}': 3, '>': 4}[c]
        autocomplete_scores.append(autocomplete_score)

    print(syntax_check_score)
    autocomplete_scores.sort()
    print(autocomplete_scores[len(autocomplete_scores)//2])
