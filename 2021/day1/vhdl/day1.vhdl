configuration day1 of sim is
	for aoc_stdio
		for dut_inst : dut
			use entity work.top generic map (
				MAX_INPUT_DIGITS => 6,
				OUTPUT_WIDTH => OUTPUT_WIDTH,
				STEP => STEP
			);
		end for;
	end for;
end configuration;
