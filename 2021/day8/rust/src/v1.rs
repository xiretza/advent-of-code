use std::collections::HashMap;

use crate::LineResult;

#[inline]
fn lookup(n: usize) -> usize {
    match n {
        17 => 1,
        25 => 7,
        30 => 4,
        34 => 2,
        37 => 5,
        39 => 3,
        41 => 6,
        42 => 0,
        49 => 8,
        45 => 9,
        _ => unreachable!(),
    }
}

#[inline]
#[must_use]
pub fn unscramble(line: &str) -> LineResult {
    let mut parts = line.split('|');
    let input = parts.next().unwrap();

    let mut counts: HashMap<_, usize> = HashMap::new();
    for c in input.chars() {
        *counts.entry(c).or_default() += 1;
    }

    let digits: Vec<_> = parts
        .next()
        .unwrap()
        .trim()
        .split(' ')
        .map(|s| s.chars().map(|c| counts[&c]).sum())
        .map(lookup)
        .collect();

    LineResult {
        unique_digits: digits.iter().filter(|d| [1, 4, 7, 8].contains(d)).count(),
        number: digits
            .iter()
            .map(|&d| char::from_digit(d as u32, 10).unwrap())
            .collect::<String>()
            .parse()
            .unwrap(),
    }
}
