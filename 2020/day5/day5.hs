import AoC

import Data.List (sort)
import Data.Maybe (fromJust)
import Numeric (readInt)
import Text.ParserCombinators.ReadP

binarify :: String -> Maybe Int
binarify = oneCompleteResult . readS_to_P $ readInt 2 (`elem` "BFLR") digitValue
    where digitValue 'F' = 0
          digitValue 'B' = 1
          digitValue 'L' = 0
          digitValue 'R' = 1

findHole :: (Enum a, Eq a) => [a] -> Maybe a
findHole (x:y:ys) | y == next = findHole $ y:ys
                  | otherwise = Just next
                  where next = succ x
findHole _ = Nothing

main = runAoC (map (fromJust . binarify) . lines) part1 part2
    where part1 = foldr1 max
          part2 = fromJust . findHole . sort
